<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'name' => 'Admin',
            'description' => Str::random(50),
        ],[
            'name' => 'User',
            'description' => Str::random(50),
        ],[
            'name' => 'Merchant',
            'description' => Str::random(50),
        ]]);
    }
}
